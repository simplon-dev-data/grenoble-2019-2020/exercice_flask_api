###############################
# EXERCICE FLASK API PARTIE 2 #
###############################
""" L'objectif de cette partie est 
de créer une API Flask qui renvoie
l'age d'une personne en fonction de sa date
de naissance """

# Importer Flask, jsonify et request de la librairie flask
# Importer une librairie pour gérer la date
# CODE A AJOUTER

# Créer une instance de Flask
# CODE A AJOUTER

# Créer une vue qui renvoie l'age d'une personne au format JSON "{"age": 25}"
# lorsque l'on envoie le paramètre "date_naissance" au format "JJ/MM/AAAA"
# sur l'url "/api" en requête GET (la validation de la donnée d'entrée doit être effectuée 
# pour les données qui ne sont pas au bon format ou les ages négatifs)
# CODE A AJOUTER


# NE PAS MODIFIER LE CODE SUIVANT
if __name__=="__main__":
    app.run()