import requests

####################################
# RECUPERATION TOKEN UTILISATEUR 1 #
####################################

url = "http://127.0.0.1:5000/login"
data = {
    "pseudo" : "utilisateur1",
    "mdp" : "password1",
}
req = requests.post(url, data = data)
token = req.json()["access_token"]

######################################
# APPEL API AVEC TOKEN UTILISATEUR 1 #
######################################

url = "http://127.0.0.1:5000/api"
headers = {
    "Authorization" : "Bearer " + token,
}
payload = {
    "date_naissance" : "12/03/1981",
}
req = requests.get(url, params=payload, headers=headers)
print(req.json())
