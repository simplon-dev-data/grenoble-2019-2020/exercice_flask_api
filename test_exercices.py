import pytest

# Test fichier 1
try:
    from ExercicePartie1_reponse import app as app1
    def test_exercice_1():
        client = app1.test_client()
        req = client.get('/api')
        assert b'Ceci est presque une API Flask' in req.data
except:
    print("Fichier 1 absent")

# Test fichier 2
try:
    from ExercicePartie2_reponse import app as app2
    def test_exercice_2_vrai_valeurs():
        client = app2.test_client()
        list_date = {
            "12/03/1983" : 37,
            "01/01/1983" : 37,
            "12/03/1901" : 119,
            "12/03/1990" : 30,
            "31/12/2000" : 19,
        }
        for date, age in list_date.items():
            req = client.get(f'/api?date_naissance={date}')
            assert req.json["age"]==age

    def test_exercice_2_erreurs():
        client = app2.test_client()
        list_erreurs = ["35/12/2012", "00/16/1559", "12/03/2025", 4, -5, "dhez"]
        for data in list_erreurs:
            req = client.get(f'/api?date_naissance={data}')
            assert req.status_code==400
except:
    print("Fichier 2 absent")

# Test fichier 3
try:
    from ExercicePartie3_reponse import app as app3
    def test_exercice_3_vrai_valeurs():
        client = app3.test_client()
        data = {
            "pseudo" : "utilisateur1",
            "mdp" : "password1",
        }
        req = client.post('/login', data = data)
        token = req.json["access_token"]
        assert req.status_code==200
        assert token is not None
        # Test answer API
        headers = {
            "Authorization" : "Bearer " + token,
        }
        req = client.get('/api?date_naissance=12/03/1981', headers=headers)
        assert req.json["age"]==39

    def test_exercice_3_erreurs():
        client = app3.test_client()
        data = {
            "pseudo" : "utilisateur1",
            "mdp" : "pwd",
        }
        req = client.post('/login', data = data)
        assert req.status_code==401
        # Test answer API
        token = "rezrerafffze"
        headers = {
            "Authorization" : "Bearer " + token,
        }
        req = client.get('/api?date_naissance=12/03/1981', headers=headers)
        assert req.status_code!=200
except:
    print("Fichier 3 absent")