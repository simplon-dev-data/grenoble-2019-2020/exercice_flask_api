###############################
# EXERCICE FLASK API PARTIE 1 #
###############################
""" L'objectif de cette partie est 
de créer une application Flask minimale"""

# Importer la class Flask de la librairie flask
# CODE A AJOUTER
from flask import Flask

# Créer une instance de Flask
# CODE A AJOUTER
app = Flask(__name__)

# Créer une vue qui renvoie le texte "Ceci est presque une API Flask" sur l'url "/api"
# CODE A AJOUTER
@app.route('/api')
def api():
    return "Ceci est presque une API Flask"

# NE PAS MODIFIER LE CODE SUIVANT
if __name__=="__main__":
    app.run()