###############################
# EXERCICE FLASK API PARTIE 3 #
###############################
""" L'objectif de cette partie est 
d'utiliser l'API de la partie et d'ajouter
une authentification """

# Importer Flask et jsonify de la librairie flask
# Importer JWTManager, jwt_required, create_access_token de la librairie flask_jwt_extended
# CODE A AJOUTER
from flask import Flask, jsonify, request
from datetime import datetime, date, timedelta
from flask_jwt_extended import JWTManager, jwt_required, create_access_token

# Créer une instance de Flask
# CODE A AJOUTER
app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'rgzggezfccaefzegf'
jwt = JWTManager(app)

# Dictionnaire de simulation de base de données utilisateurs
bdd = {
    "utilisateur1" : "password1",
    "utilisateur2" : "password2",
    "utilisateur3" : "password3",
}

# Créer une vue qui renvoie l'age d'une personne au format JSON "{"age": 25}"
# lorsque l'on envoie le paramètre "date_naissance" au format "JJ/MM/AAAA"
# sur l'url "/api" en requête GET (la validation de la donnée d'entrée doit être effectuée)
# CODE A AJOUTER
@app.route('/api')
@jwt_required
def api():
    date_naissance = request.args.get('date_naissance')
    try:
        date_naissance = datetime.strptime(date_naissance, '%d/%m/%Y').date()
    except ValueError:
        return jsonify(message="La date de naissance doit être au format JJ/MM/AAAA"), 400
    age = (date.today() - date_naissance) // timedelta(days=365.2425)
    if age<0:
        return jsonify(message="La date de naissance doit être antérieure à aujourd'hui"), 400
    else:
        return jsonify(age=age)

@app.route('/login', methods=['POST'])
def login():
    pseudo = request.form['pseudo']
    mdp = request.form['mdp']
    if bdd[pseudo]==mdp:
        access_token = create_access_token(identity=pseudo)
        return jsonify(message="Authentification réussie !", access_token=access_token)
    else:
        return jsonify(message="Problème d'authentification"), 401

# NE PAS MODIFIER LE CODE SUIVANT
if __name__=="__main__":
    app.run()