###############################
# EXERCICE FLASK API PARTIE 2 #
###############################
""" L'objectif de cette partie est 
de créer une API Flask qui renvoie
l'age d'une personne en fonction de sa date
de naissance """

# Importer Flask et jsonify de la librairie flask
# CODE A AJOUTER
from flask import Flask, jsonify, request
from datetime import datetime, date, timedelta

# Créer une instance de Flask
# CODE A AJOUTER
app = Flask(__name__)

# Créer une vue qui renvoie l'age d'une personne au format JSON "{"age": 25}"
# lorsque l'on envoie le paramètre "date_naissance" au format "JJ/MM/AAAA"
# sur l'url "/api" en requête GET (la validation de la donnée d'entrée doit être effectuée)
# CODE A AJOUTER
@app.route('/api')
def api():
    date_naissance = request.args.get('date_naissance')
    try:
        date_naissance = datetime.strptime(date_naissance, '%d/%m/%Y').date()
    except ValueError:
        return jsonify(message="La date de naissance doit être au format JJ/MM/AAAA"), 400
    age = (date.today() - date_naissance) // timedelta(days=365.2425)
    if age<0:
        return jsonify(message="La date de naissance doit être antérieure à aujourd'hui"), 400
    else:
        return jsonify(age=age)

# NE PAS MODIFIER LE CODE SUIVANT
if __name__=="__main__":
    app.run()