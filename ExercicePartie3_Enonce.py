###############################
# EXERCICE FLASK API PARTIE 3 #
###############################
""" L'objectif de cette partie est 
d'utiliser l'API de la partie et d'ajouter
une authentification """

# Importer Flask, jsonify et request de la librairie flask
# Importer une librairie pour gérer la date
# Importer JWTManager, jwt_required, create_access_token de la librairie flask_jwt_extended
# CODE A AJOUTER

# Créer une instance de Flask
app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'rgzggezfccaefzegf'
jwt = JWTManager(app)

# Dictionnaire de simulation de base de données utilisateurs
bdd = {
    "utilisateur1" : "password1",
    "utilisateur2" : "password2",
    "utilisateur3" : "password3",
}

# Créer une vue qui renvoie l'age d'une personne au format JSON "{"age": 25}"
# lorsque l'on envoie le paramètre "date_naissance" au format "JJ/MM/AAAA"
# sur l'url "/api" en requête GET (la validation de la donnée d'entrée doit être effectuée)
# CODE A AJOUTER
@app.route('/api')
@jwt_required
def api():
    # MEME CODE QUE LA PARTIE 2
    pass

# Créer une vue qui permet de récupérer un token en envoyant
# une requête POST avec le pseudo et le mot de passe définis dans
# le dictionnaire bdd
@app.route('/login', methods=['POST'])
def login():
    pass

# NE PAS MODIFIER LE CODE SUIVANT
if __name__=="__main__":
    app.run()