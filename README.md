# Flask_API
[![pipeline status](https://gitlab.com/simplon-dev-data/exercice_flask_api/badges/master/pipeline.svg)](https://gitlab.com/simplon-dev-data/exercice_flask_api/commits/master)

Ce repository contient un exercice en 3 parties :
* Partie 1 : construire une application Flask minimale
* Partie 2 : construire une API qui renvoie l'âge d'une personne connaissant sa date de naissance
* Partie 3 : ajouter une authentification par token (JWT) à l'API de la partie 2

Le fichier Code_request_token.py vous sert à vérifier la validité de votre code de la partie 3.

Pour répondre aux exercices, vous devez travailler sur la branche de votre prénom. Pour cloner uniquement **votre branche**, tapez la commande suivante :
`git clone --branch <votre prenom> https://gitlab.com/simplon-dev-data/exercice_flask_api.git`

Les fichiers de réponses doivent être nommés de la façon suivante pour pouvoir passer les tests :
* ExercicePartie1_reponse.py
* ExercicePartie2_reponse.py
* ExercicePartie3_reponse.py

Pour envoyer la réponse sur le serveur, vous tapez :
`git push origin <votre prenom>`

Lorsque les tests sont en cours, vous voyez un logo marron pipeline running

Sinon votre code est correct, à la fin des tests vous voyez un logo vert pipeline passed

S'il y a une erreur, vous voyez un logo rouge pipeline failed